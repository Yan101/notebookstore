﻿using System.Collections.Generic;
using NoteBookStore.Domain.Abstract;
using NoteBookStore.Domain.Entities;

namespace NoteBookStore.Domain.Concrete
{
    //2. Создание хранилища для объектов NoteBook
    public class EFNoteBookRepository : INoteBookRepository
    {
        EFDbContext context = new EFDbContext();

        public IEnumerable<NoteBook> NoteBooks
        {
            get { return context.NoteBooks; }
        }
    }
}
