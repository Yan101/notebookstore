﻿using NoteBookStore.Domain.Entities;
using System.Data.Entity;

namespace NoteBookStore.Domain.Concrete
{
    //1. класс контекста, который будет ассоциировать модель с базой данных.
    public class EFDbContext : DbContext
    {
        public DbSet<NoteBook> NoteBooks { get; set; }
    }
}
