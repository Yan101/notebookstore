﻿using System.Collections.Generic;
using NoteBookStore.Domain.Entities;

namespace NoteBookStore.Domain.Abstract
{
    /*Этот интерфейс использует интерфейс IEnumerable<T>, чтобы позволить вызывающему коду получать последовательность объектов Game, ничего не сообщая о том, как или где хранятся или извлекаются данные. 
    Класс, зависящий от интерфейса INoteBookRepository, может получать объекты NoteBook, ничего не зная о том, откуда они поступают или каким образом класс реализации будет их доставлять. В этом и состоит суть шаблона хранилища. */
    public interface INoteBookRepository
    {
        IEnumerable<NoteBook> NoteBooks { get; }
    }
}
