﻿namespace NoteBookStore.Domain.Entities
{
    public class NoteBook
    {
        public int NoteBookId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Category { get; set; }
        public decimal Price { get; set; }
    }
}
