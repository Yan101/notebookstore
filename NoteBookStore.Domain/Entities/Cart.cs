﻿using System.Collections.Generic;
using System.Linq;

namespace NoteBookStore.Domain.Entities
{
    public class Cart
    {
        private List<CartLine> lineCollection = new List<CartLine>();

        public void AddItem(NoteBook noteBook, int quantity)
        {
            //ищем в корзине переданный товар
            CartLine line = lineCollection
                .Where(n => n.NoteBook.NoteBookId == noteBook.NoteBookId)
                .FirstOrDefault();

            //если нету, тогда добавим в корзину,
            if (line == null)
            {
                lineCollection.Add(
                    new CartLine
                    {
                        NoteBook = noteBook,
                        Quantity = quantity
                    });
            }
            else
            {
                line.Quantity += quantity; // иначе просто плюсанем количество.
            }

        }

        public void RemoveLine(NoteBook noteBook)
        {
            // вначале будем уменьшать количество товаров.
            CartLine line = lineCollection
                .Where(n => n.NoteBook.NoteBookId == noteBook.NoteBookId)
                .FirstOrDefault();

            if (line.Quantity > 1)
            {
                --line.Quantity;
            }
            else
            {
                //удалить окончательно
                lineCollection.RemoveAll(n => n.NoteBook.NoteBookId == noteBook.NoteBookId);
            }
        }

        public decimal ComputeTotalValue()
        {
            return lineCollection.Sum(n => n.NoteBook.Price * n.Quantity);
        }

        public void Clear()
        {
            lineCollection.Clear();
        }

        public IEnumerable<CartLine> Lines
        {
            get { return lineCollection; }
        }
    }

    public class CartLine
    {
        public NoteBook NoteBook { get; set; }
        public int Quantity { get; set; }
    }
}