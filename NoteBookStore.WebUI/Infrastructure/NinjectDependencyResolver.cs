﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ninject;
using NoteBookStore.Domain.Concrete;
using NoteBookStore.Domain.Abstract;

namespace NoteBookStore.WebUI.Infrastructure
{
    public class NinjectDependencyResolver : IDependencyResolver
    {
        private IKernel kernel;

        public NinjectDependencyResolver(IKernel kernelParam)
        {
            kernel = kernelParam;
            AddBindings();
        }

        public object GetService(Type serviceType)
        {
            return kernel.TryGet(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            return kernel.GetAll(serviceType);
        }

        //Новая привязка указывает Ninject о том, что для обслуживания запросов к интерфейсу IGameRepository необходимо создавать экземпляры класса EFGameRepository. 
        private void AddBindings() // Здесь размещаются привязки
        {
            kernel.Bind<INoteBookRepository>().To<EFNoteBookRepository>();
        }
    }
}