﻿using System;
using System.Web.Mvc;
using NoteBookStore.Domain.Entities;

namespace NoteBookStore.WebUI.Infrastructure.Binders
{
    // Создание специального связывателя модели - System.Web.Mvc.IModelBinder. 
    /*Нам нравится использовать средство состояния сеанса в контроллере Cart (Session) для хранения и управления объектами Cart, созданными в предыдущей статье, но 
    он не вписывается в остальные части модели приложения, которые основаны на параметрах методов действий. 
    Мы не можем провести исчерпывающее модульное тестирование класса CartController до тех пор, пока не построим имитацию параметра Session базового класса, 
    а это означает имитацию класса Controller и множество других вещей, с которыми лучше не связываться.
    Для решения этой проблемы мы создадим специальный связыватель модели, который будет получать объект Cart, содержащийся внутри данных сеанса. 
    В результате у инфраструктуры MVC Framework появится возможность создавать объекты Cart и передавать их в виде параметров методам действий класса CartController.*/

    /*!!! Инфраструктуре MVC Framework необходимо сообщить о том, что она может использовать класс CartModelBinder для создания экземпляров Cart. Это делается в методе Application_Start() внутри файла Global.asax*/

    // IModelBinder: BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
    //1. Параметр ControllerContext обеспечивает доступ ко всей информации, которой располагает класс контроллера, включая детали запроса от клиента. 
    //2. Параметр ModelBindingContext предоставляет сведения об объекте модели, который требуется создать, а также набор инструментов для упрощения процесса привязки.
    public class CartModelBinder : IModelBinder
    {
        private const string sessionKey = "Cart";

        public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
        {
            Cart cart = null;

            //получить объект Cart из сеанса. В рассматриваемом случае нас интересует класс ControllerContext. Он имеет свойство HttpContext, которое, в свою очередь, содержит свойство Session, позволяющее получать и устанавливать данные сеанса. Объект Cart получается за счет чтения значения для ключа из данных сеанса и создания экземпляра Cart, если оказалось, что он не существует.
            if (controllerContext.HttpContext.Session != null)
            {
                cart = (Cart)controllerContext.HttpContext.Session[sessionKey];
            }

            // Создать объект Cart, если он не обнаружен в сеансе
            if(cart == null)
            {
                cart = new Cart();
                if(controllerContext.HttpContext.Session != null)
                {
                    controllerContext.HttpContext.Session[sessionKey] = cart;
                }
            }

            // Возвратить объект Cart
            return cart;
        }
    }
}
 