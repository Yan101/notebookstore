// <auto-generated />
namespace NoteBookStore.WebUI.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class tst : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(tst));
        
        string IMigrationMetadata.Id
        {
            get { return "201610070556535_tst"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
