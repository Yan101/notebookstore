namespace NoteBookStore.WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPathAvatarToUsers7 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "MyAvatarPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "MyAvatarPath");
        }
    }
}
