namespace NoteBookStore.WebUI.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMyUserName3_OK : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "MyUserName", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "MyUserName");
        }
    }
}
