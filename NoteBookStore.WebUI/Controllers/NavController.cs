﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NoteBookStore.Domain.Abstract;
using NoteBookStore.WebUI.Models;

namespace NoteBookStore.WebUI.Controllers
{
    public class NavController : Controller
    {
        private INoteBookRepository repository;
        public NavController(INoteBookRepository repo)
        {
            repository = repo;
        }
        public PartialViewResult Menu(string category = null)
        {
            NoteBooksCategoryViewModel model = new NoteBooksCategoryViewModel
            {
                //вывод всех категорий.
                Categories = repository.NoteBooks
                .Select(notebook => notebook.Category)
                .Distinct()
                .OrderBy(x => x),
                SelectedCategory = category
            };
            return PartialView(model);
        }
    }
}