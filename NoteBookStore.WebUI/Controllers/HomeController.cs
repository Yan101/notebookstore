﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using NoteBookStore.WebUI.Models;
using System.Web;
using System.Web.Mvc;

namespace NoteBookStore.WebUI.Controllers
{
    public class HomeController : Controller
    {
        public ApplicationUserManager UserManager // // // регистрация
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }
        
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult testIndex()
        {
            return View();
        }

        // // //Удаленная проверка. http://professorweb.ru/my/ASP_NET/mvc/level7/7_6.php
        [HttpPost]
        public JsonResult ValidateEmailRegister(string email) //В дополнение к возврату такого результата, методы действий для проверки достоверности должны определять параметр, который имеет то же самое имя, что и проверяемое поле данных: в рассматриваемом примере это Date. 
        {
            ApplicationUser existEmail = new ApplicationUser(); //Email
            existEmail = UserManager.FindByEmail(email);
            if (existEmail != null) //есть
                return Json("Client: Такой Email, уже есть!", JsonRequestBehavior.AllowGet);
            else //нету
                return Json(true, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidateEmailLogin(string email)
        {
            ApplicationUser existEmail = new ApplicationUser();
            existEmail = UserManager.FindByEmail(email);
            if (existEmail != null) 
                return Json(true, JsonRequestBehavior.AllowGet);
            else 
                return Json("Client: Такого Email'a нет!", JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult ValidatePasswordConfirmRegister(string passwordConfirm, string password)
        {
            if (password == passwordConfirm)
                return Json(true, JsonRequestBehavior.AllowGet);
            else 
                return Json("Client: Пароли не совпадают!", JsonRequestBehavior.AllowGet);
        }
    }
}