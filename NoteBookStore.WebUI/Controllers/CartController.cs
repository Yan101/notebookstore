﻿using System.Linq;
using System.Web.Mvc;
using NoteBookStore.Domain.Abstract;
using NoteBookStore.Domain.Entities;
using NoteBookStore.WebUI.Models;
using System.Web.Routing;

namespace NoteBookStore.WebUI.Controllers
{
    public class CartController : Controller
    {
        private INoteBookRepository repository;
        public CartController(INoteBookRepository repo)
        {
            repository = repo;
        }

        // // // Cart cart - этот объект передевать методам не нужно, его подгонит ModelBinder автоматом.
        public ViewResult Index(Cart cart, string returnUrl) //для отображения содержимого Cart. 
        { 
            return View(
                new CartIndexViewModel
                {
                    Cart = cart,
                    ReturnUrl = returnUrl
                });
        }

        public ActionResult AddToCart(Cart cart, int noteBookId, string returnUrl)
        {
            NoteBook notebook = repository.NoteBooks
                .FirstOrDefault(n => n.NoteBookId == noteBookId);

            if (notebook != null)
            {
                cart.AddItem(notebook, 1);
            }
            return Redirect(returnUrl);
        }

        public RedirectToRouteResult RemoveFromCart(Cart cart, int noteBookId, string returnUrl)
        {
            NoteBook notebook = repository.NoteBooks
               .FirstOrDefault(n => n.NoteBookId == noteBookId);

            if (notebook != null)
            {
                cart.RemoveLine(notebook);
            }
            return RedirectToAction("Index", new { returnUrl });
        }

        public ActionResult RemoveAllFromCart(Cart cart, string returnUrl)
        {
            cart.Clear();
            return Redirect( returnUrl );
        }

        public PartialViewResult Summary(Cart cart) //Метод должен визуализировать представление, передавая в качестве данных представления текущий объект Cart (который будет получен с использованием специального связывателя модели).
        {
            return PartialView(cart);
        }
    }
}