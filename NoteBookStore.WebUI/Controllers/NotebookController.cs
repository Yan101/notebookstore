﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NoteBookStore.Domain.Abstract;
using NoteBookStore.Domain.Entities;
using NoteBookStore.WebUI.Models;

namespace NoteBookStore.WebUI.Controllers
{
    public class NoteBookController : Controller
    {
        private INoteBookRepository repository;
        public int pageSize = 12;
        public NoteBookController(INoteBookRepository repo)
        {
            repository = repo;
        }

        public ViewResult List(string category, int page = 1) //http://localhost:53985/NoteBook?page=2
        {
            //отдаем представлению нашу промежуточную модель, в ней есть 1. страница с ноутами на нужной странице и 2. список всех страниц в виде ссылок.
            NoteBooksListViewModel model = new NoteBooksListViewModel
            {
                //делим все товары по страницам и отбираем те для которых указана страница.
                NoteBooks = repository.NoteBooks
                    .Where(n => category == null || n.Category == category) //категория. Здесь два условия выбора!! Если null Тогда все выбрать, иначе = category
                    .OrderBy(notebook => notebook.NoteBookId)
                    .Skip((page - 1) * pageSize)
                    .Take(pageSize),
                PagingInfo = new PagingInfo
                {
                    CurrentPage = page,
                    ItemsPerPage = pageSize,
                    TotalItems = category == null ? repository.NoteBooks.Count() : repository.NoteBooks.Where(notebook => notebook.Category == category).Count() 
                },
                CurrentCategory = category
            };
            return View(model);
        }
    }
}