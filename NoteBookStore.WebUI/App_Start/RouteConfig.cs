﻿using System.Web.Mvc;
using System.Web.Routing;

namespace NoteBookStore.WebUI
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            // "/" - Выводит первую страницу списка товаров всех категорий
            routes.MapRoute( 
                name: null,
                url: "",
                defaults: new { controller = "NoteBook", action = "List", category = (string)null, page = 1 }
            );

            // "/Page2" - Выводит указанную страницу (в этом случае страницу 2), отображая товары всех категорий
            routes.MapRoute( 
                name: null,
                url: "Page{page}",
                defaults: new { controller = "NoteBook", action = "List", category = (string)null },
                constraints: new { page = @"\d+" } //Ограничения параметров типа. Ввод только чисел 0-9
            );

            // "/Симулятор" - Отображает первую страницу элементов указанной категории (в этом случае игры в разделе "Симуляторы")
            routes.MapRoute(
               name: null,
               url: "{category}",
               defaults: new { controller = "NoteBook", action = "List", page = 1 } //стнд начения
            );

            // "/Симулятор/Page2" - Отображает заданную страницу (в этом случае страницу 2) элементов указанной категории (Симулятор)
            routes.MapRoute( //!!!!!!
                name: null,
                url: "{category}/Page{page}",
                defaults: new { controller = "NoteBook", action = "List" },
                constraints: new { page = @"\d+" }
            );

            //стнд. Удалить? из-за категорий не будет работать... проверить для др функция лог/рег...!!!
            /*routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "NoteBook", action = "List", id = UrlParameter.Optional }
            );*/

            routes.MapRoute(
                name: null,
                url: "{controller}/{action}"
            );
        }
    }
}