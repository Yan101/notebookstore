﻿using System.Web;
using System.Web.Optimization;

namespace NoteBookStore.WebUI
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery.unobtrusive-ajax").Include(
                        "~/Scripts/jquery.unobtrusive-ajax*"));

            bundles.Add(new ScriptBundle("~/bundles/jquery_ui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                        "~/Scripts/bootstrap.js",
                        "~/Scripts/respond.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/MyJS").Include(
                        "~/Scripts/MyJS.js"));

            bundles.Add(new ScriptBundle("~/bundles/mCustomScrollbar").Include(
                        "~/Scripts/jquery.mCustomScrollbar.concat.min.js"));

            // // // CSS // // //

            bundles.Add(new StyleBundle("~/Content/css_bootstrap").Include(
                      "~/Content/bootstrap*",
                      "~/Content/bootstrap-*"));

            bundles.Add(new StyleBundle("~/Content/css_MyCssMarkUp").Include(
                        "~/Content/_MyCssMarkUp*"));

            bundles.Add(new StyleBundle("~/Content/css_mCustomScrollbar").Include(
                      "~/Content/jquery.mCustomScrollbar.css",
                      "~/Content/style_mCustomScrollbar.css"));
        }
    }
}
