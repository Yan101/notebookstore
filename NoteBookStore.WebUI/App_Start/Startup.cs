﻿using Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.AspNet.Identity;
using NoteBookStore.WebUI.Models;

[assembly: OwinStartup(typeof(NoteBookStore.WebUI.App_Start.Startup))]

namespace NoteBookStore.WebUI.App_Start
{
    public class Startup
    {
        //Интерфейс IAppBuilder определяет множество методов, в данном случае нам достаточно трех методов
        public void Configuration(IAppBuilder app)
        {
            // настраиваем контекст и менеджер
            app.CreatePerOwinContext<ApplicationContext>(ApplicationContext.Create); //Метод CreatePerOwinContext регистрирует в OWIN класс контекста ApplicationContext.
            app.CreatePerOwinContext<ApplicationUserManager>(ApplicationUserManager.Create); //и менеджер пользователей ApplicationUserManager
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
                LoginPath = new PathString("/Account/Login") //адрес URL, по которому будут перенаправляться неавторизованные пользователи
            });
        }
    }
}
