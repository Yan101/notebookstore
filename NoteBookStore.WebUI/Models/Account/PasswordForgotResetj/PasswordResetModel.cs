﻿using System.ComponentModel.DataAnnotations;

namespace NoteBookStore.WebUI.Models
{
    public class PasswordResetModel
    {
        [Required(ErrorMessage = "Необходимо ввести E-mail")]
        [Display(Name = "Введите E-mail:")]
        [EmailAddress(ErrorMessage = "Неправильный тип Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Необходимо ввести Пароль")]
        [Display(Name = "Введите Новый Пароль:")]
        [DataType(DataType.Password)]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        public string Password { get; set; }

        [Required(ErrorMessage = "Необходимо подтвердить пароль")]
        [Display(Name = "Подтвердите новый пароль:")]
        [DataType(DataType.Password)]
        [Compare("Password", ErrorMessage = "Пароли не совпадают")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }
}