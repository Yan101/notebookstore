﻿using System.ComponentModel.DataAnnotations;

namespace NoteBookStore.WebUI.Models
{
    public class PasswordForgotModel
    {
        [Required(ErrorMessage = "Необходимо ввести E-mail")]
        [Display(Name = "Введите E-mail:")]
        [EmailAddress(ErrorMessage = "Неправильный тип Email")]
        public string Email { get; set; }
    }
}