﻿using Microsoft.Owin;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;

using NoteBookStore.WebUI.ServiceEmail;
using System.Data.Entity;

namespace NoteBookStore.WebUI.Models
{
    //основные поля наследуются из классов, дополнительные поля дописываем сами.
    public class ApplicationUser : IdentityUser //AspNetUsers - таблица
    {
        public ApplicationUser() { }
        public string MyUserName { get; set; }
        public string MyAvatarPath { get; set; }
    }

    public class ApplicationUserLogins : IdentityUserLogin //AspNetUserLogins - таблица
    {
        public ApplicationUserLogins() { }
    }

    //1. контекст
    public class ApplicationContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationContext() : base("IdentityDb")
        {
            Database.SetInitializer(new DropCreateDatabaseIfModelChanges<ApplicationContext>());
        }
        public static ApplicationContext Create()
        {
            return new ApplicationContext();
        }
        public DbSet<ApplicationUserLogins> Logins { get; set; } //AspNetUserLogins - доступ
    }

    public class ApplicationUserManager : UserManager<ApplicationUser>
    {
        //принимает объект хранилища пользователей IUserStore
        public ApplicationUserManager(IUserStore<ApplicationUser> store) : base(store) { }

        //создает экземпляр класса ApplicationUserManager с помощью объекта контекста IOwinContext
        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            ApplicationContext db = context.Get<ApplicationContext>();
            ApplicationUserManager manager = new ApplicationUserManager(new UserStore<ApplicationUser>(db));

            // // //Configure validation logic for passwords
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            manager.EmailService = new EmailService();

            var dataProtectionProvider = options.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<ApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
            }

            return manager;
        }
    }
}