﻿using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace NoteBookStore.WebUI.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Необходимо ввести E-mail")]
        [Display(Name = "Введите E-mail:")]
        [EmailAddress(ErrorMessage = "Invalid Email Address")]
        [Remote("ValidateEmailLogin", "Home", HttpMethod = "POST")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Необходимо ввести Пароль")]
        [Display(Name = "Введите Пароль:")]
        [DataType(DataType.Password)]
        public string Password { get; set; }
    }
}