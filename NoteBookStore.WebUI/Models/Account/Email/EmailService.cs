﻿using Microsoft.AspNet.Identity;
using System.Threading.Tasks;

using System.Net;
using System.Net.Mail;

namespace NoteBookStore.WebUI.ServiceEmail
{
    public class EmailService : IIdentityMessageService
    {
        public Task SendAsync(IdentityMessage message)
        {
            //настройка логина, пароля отправителяz
            var from = "linkinyan@yandex.by";
            var pass = "sdenby33241";

            //адрес и порт smtp - сервера, с которого мы и будем отправлять письмо
            SmtpClient client = new SmtpClient("smtp.yandex.ru", 25);

            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false; //управляет отправкой учетных данных DefaultCredentials вместе с запросами.
            client.Credentials = new NetworkCredential(from, pass); //Возвращает или задает учетные данные, используемые для проверки подлинности отправителя.
            client.EnableSsl = true; //Указывает, использует ли SmtpClient протокол SSL для шифрования подключения.

            // создаем письмо: message.Destination - адрес получателя
            var mail = new MailMessage(from, message.Destination); //Представляет сообщение электронной почты, которое может быть отправлено с помощью класса SmtpClient.
            mail.Subject = message.Subject; //Получает или задает строку темы для данного сообщения электронной почты.
            mail.Body = message.Body; //Получает или задает основную часть сообщения.
            mail.IsBodyHtml = true; //Получает или задает значение, показывающее, имеет ли основная часть почтового сообщения формат HTML.

            return client.SendMailAsync(mail); //Отправляет указанное сообщение SMTP-серверу для доставки в качестве асинхронной операции.9+
        }
    }
}