﻿using System;

namespace NoteBookStore.WebUI.Models
{
    public class PagingInfo
    {
        public int TotalItems { get; set; } // Кол-во товаров
        public int ItemsPerPage { get; set; } // Кол-во товаров на одной странице
        public int CurrentPage { get; set; } // Номер текущей страницы
        public int TotalPages // Общее кол-во страниц
        {
            get { return (int)Math.Ceiling((decimal)TotalItems / ItemsPerPage); } //Math.Ceiling - Возвращает наименьшее целое число, которое больше или равно заданному числу. Т.е. всего 5 страниц по 4 товара , получим 5/4=1.25  Итого 2 страницы!
        }
    }
}