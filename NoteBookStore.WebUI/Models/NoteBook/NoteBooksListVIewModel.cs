﻿using System.Collections.Generic;
using NoteBookStore.Domain.Entities;

namespace NoteBookStore.WebUI.Models
{
    //Класс для PageLinks.
    //Нужно также предоставить представлению экземпляр класса модели представления PagingInfo. 
    //Это можно было бы сделать с применением объекта ViewBag, но предпочтительнее поместить все данные, которые требуется передать из контроллера представлению, в один класс модели представления.
    public class NoteBooksListViewModel
    {
        public IEnumerable<NoteBook> NoteBooks { get; set; }
        public PagingInfo PagingInfo { get; set; }
        public string CurrentCategory { get; set; }
    }
}