﻿using System.Collections.Generic;

namespace NoteBookStore.WebUI.Models
{
    public class NoteBooksCategoryViewModel
    {
        public IEnumerable<string> Categories { get; set; }
        public string SelectedCategory { get; set; }
    }
}