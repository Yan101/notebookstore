﻿using System;
using System.Text;
using System.Web.Mvc;
using NoteBookStore.WebUI.Models;

namespace NoteBookStore.WebUI.HtmlHelpers
{
    public static class PagingHelper
    {
        //MvcHtmlString Представляет HTML-кодированную строку, которую не следует перекодировать еще раз.
        //Расширяющий метод PageLinks() генерирует HTML-разметку для набора ссылок на страницы с использованием информации, предоставленной в объекте PagingInfo. Параметр Func принимает делегат, который применяется для генерации ссылок, обеспечивающих просмотр других страниц.
        public static MvcHtmlString PageLinks(this HtmlHelper html, PagingInfo pagingInfo, Func<int, string> pageUrl)
        {
            StringBuilder result = new StringBuilder();
            for (int i = 1; i <= pagingInfo.TotalPages; i++)
            {
                /*
                   padding-top: 12px; padding-bottom: 0; padding-left: 0.5rem; width: 38px;
                 */
                TagBuilder tag = new TagBuilder("a");   //<a></a>
                tag.MergeAttribute("href", pageUrl(i)); //<a href ="1"></a>
                tag.InnerHtml = i.ToString();           //<a href ="1">1</a>

                tag.MergeAttribute("style", "margin-right:6px; padding-top:16px; padding-left:0px; height:38px; width:38px; font-size:0.875rem;");

                if (i == pagingInfo.CurrentPage) //<a class=""btn btn-default btn-primary selected"" href=""Page2"">2</a>
                {
                    tag.AddCssClass("btn-info");
                }
                else
                {
                    tag.AddCssClass("btn-outline-info");
                }

                tag.AddCssClass("btn");

                //<a class=""btn btn-default"" href ="1">1</a>
                result.Append(tag.ToString());         
            }
            return MvcHtmlString.Create(result.ToString());
        }
    }
}