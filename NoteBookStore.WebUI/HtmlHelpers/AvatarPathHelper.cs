﻿using NoteBookStore.WebUI.Controllers;
using NoteBookStore.WebUI.Models;
using System;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace NoteBookStore.WebUI.HtmlHelpers
{
    public static class AvatarPathHelper
    {
        public static string returnAvatarPath(this HtmlHelper helper, IPrincipal userName) //http://www.cyberforum.ru/asp-net-mvc/thread617793.html
        {
            string tempUserName = userName.Identity.Name;

            using (var context = new ApplicationContext())
            {
                try
                {
                    var user = (from b in context.Users
                                where b.UserName == tempUserName == true
                                select b).FirstOrDefault();
                    return user.MyAvatarPath;
                }
                catch (NullReferenceException)
                {
                    return AccountController.noImageAvatarPath;
                }
            }
        }
    }
}