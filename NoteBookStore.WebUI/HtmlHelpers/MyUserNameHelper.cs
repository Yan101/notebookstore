﻿using NoteBookStore.WebUI.Models;
using System;
using System.Linq;
using System.Security.Principal;
using System.Web.Mvc;

namespace NoteBookStore.WebUI.HtmlHelpers
{
    public static class MyUserNameHelper
    {
        public static string returnMyUserName(this HtmlHelper helper, IPrincipal userName)
        {
            string tempUserName = userName.Identity.Name;

            using (var context = new ApplicationContext())
            {
                try
                {
                    var user = (from b in context.Users
                                where b.UserName == tempUserName == true
                                select b).FirstOrDefault();
                    return user.MyUserName;
                }
                catch (NullReferenceException)
                {
                    return "null";
                }
            }
        }

       
    }
}